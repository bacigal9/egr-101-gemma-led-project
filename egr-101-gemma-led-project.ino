// Library for interfacing with the LED strip.
#include <Adafruit_NeoPixel.h>

// Constants
// The digital I/O pin number connected to the LED strip.
#define PIN       2
// The number of LEDs in the LED strip.
#define NUM_LEDS 12

// Alias for Adafruit_NeoPixel's accepted representation of colors
#define Color uint32_t

// How many "source" (non-trail) LEDs are on at one time.
// Odd numbers are probably not a good idea, the static_assert will error if this and TRAIL_LENGTH's product is too big.
#define SPREAD_NUM_LEDS 2
// How many "trail" LEDs are following each source.
// the static_assert will error if this and SPREAD_NUM_LEDS' product is too big.
#define TRAIL_LENGTH 5
// How often the lights' animation is updated.
// Should have minimal effect on the hue's animation speed, but will affect that of the offset animation.
#define FRAME_TIME 100
// How fast the hue animation is.
// In units/millisecond.
#define HUE_SPEED 9

static_assert((TRAIL_LENGTH + 1) * SPREAD_NUM_LEDS <= NUM_LEDS, "Attempting to use more lights than the strip has!");

// Globals
uint8_t offset = 0;          // Position of spinner animation
uint32_t prevTime;           // Time of last frame change
uint16_t hue = 0;

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);

void hold_color(Color color, unsigned long time) {
  pixels.fill(color);
  pixels.show();
  delay(time);
}

void set_led_spread(Color color, int local_offset = 0) {
  // Account for negative numbers
  if (local_offset < 0) local_offset = NUM_LEDS + local_offset;

  for (size_t i = 0; i < SPREAD_NUM_LEDS; i++) {
    pixels.setPixelColor((i * (NUM_LEDS / SPREAD_NUM_LEDS) + offset + local_offset) % NUM_LEDS, color);
  }
}

// Only runs once at startup.
void setup() {
  // Initialize pixels
  pixels.begin();
  pixels.setBrightness(60);
  // Initialize timer
  prevTime = millis() - FRAME_TIME;

  // Color check
  hold_color(0xFF0000, 16);
  hold_color(0x00FF00, 16);
  hold_color(0x0000FF, 16);
  hold_color(0, 20);
}

// Runs continously after setup.
void loop() {
  uint32_t t;
  uint16_t hue;

  t = millis();
  hue = (t * HUE_SPEED) % 0xFFFF;
  // If we've gone through all of the previous frame's time:
  if (t - prevTime > FRAME_TIME) {
    // Reset the timer
    prevTime = t;
    // Increment the offset
    offset = (offset + 1) % (NUM_LEDS * SPREAD_NUM_LEDS);
    // Clear buffer
    pixels.fill(0);
    // Set buffer for current frame by looping through trail length..
    for (uint8_t i = 0; i <= TRAIL_LENGTH + 1; i++) {
      // ..and then set each spread of similarly-colored lights individually
      set_led_spread(Adafruit_NeoPixel::gamma32(Adafruit_NeoPixel::ColorHSV(hue, 255, (255 / (TRAIL_LENGTH + 2)) * ((TRAIL_LENGTH + 2) - i))), 0 - i);
    }
    // Then push pixel buffer
    pixels.show();
  }
}
